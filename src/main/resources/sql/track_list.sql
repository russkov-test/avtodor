with TrackPoints as (select t.id                                                          as track_id,
                            p.point_datetime,
                            p.lat                                                         as point_lat,
                            p.lon                                                         as point_lon,
                            t.video_id,
                            t.video_creation_date,
                            t.device_id,
                            LAG(p.lat) over (partition by t.id order by p.point_datetime) as prev_point_lat,
                            LAG(p.lon) over (partition by t.id order by p.point_datetime) as prev_point_lon
                     from track t
                              left join point p on t.video_id = p.video_id)
select track_id,
       video_id,
       video_creation_date,
       device_id,
       EXTRACT(epoch from (MAX(point_datetime) - MIN(point_datetime))) as track_duration_seconds,
       SUM(1000 * 6371 * (
               2 * ATAN2(
                   SQRT(
                                   SIN(RADIANS((point_lat - prev_point_lat) / 2)) ^ 2 +
                                   COS(RADIANS(prev_point_lat)) * COS(RADIANS(point_lat)) *
                                   SIN(RADIANS((point_lon - prev_point_lon) / 2)) ^ 2
                       ),
                   SQRT(1 - SIN(RADIANS((point_lat - prev_point_lat) / 2)) ^ 2 +
                        COS(RADIANS(prev_point_lat)) * COS(RADIANS(point_lat)) *
                        SIN(RADIANS((point_lon - prev_point_lon) / 2)) ^ 2
                       )
               )
           ))                                                          as total_distance_meters,
       case
           when EXTRACT(epoch from (MAX(point_datetime) - MIN(point_datetime))) > 0 then
                   (SUM(1000 * 6371 * (
                           2 * ATAN2(
                               SQRT(
                                               SIN(RADIANS((point_lat - prev_point_lat) / 2)) ^ 2 +
                                               COS(RADIANS(prev_point_lat)) * COS(RADIANS(point_lat)) *
                                               SIN(RADIANS((point_lon - prev_point_lon) / 2)) ^ 2
                                   ),
                               SQRT(1 - SIN(RADIANS((point_lat - prev_point_lat) / 2)) ^ 2 +
                                    COS(RADIANS(prev_point_lat)) * COS(RADIANS(point_lat)) *
                                    SIN(RADIANS((point_lon - prev_point_lon) / 2)) ^ 2
                                   )
                           )
                       ))) / EXTRACT(epoch from (MAX(point_datetime) - MIN(point_datetime)))
           end                                                         as average_velocity
from TrackPoints
group by track_id, video_id, video_creation_date, device_id
order by track_id;