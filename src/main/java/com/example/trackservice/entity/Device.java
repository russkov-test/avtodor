package com.example.trackservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "device")
public class Device {
    @Id
    @Column(name = "device_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String deviceId;

    @Column(name = "import_date")
    private LocalDateTime importDate;

    @Column(name = "description")
    private String description;

}
