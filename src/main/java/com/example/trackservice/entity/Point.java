package com.example.trackservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "point")
public class Point {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lon")
    private Double lon;

    @Column(name = "bearing")
    private Double bearing;

    @Column(name = "velocity")
    private Double velocity;

    @Column(name = "point_datetime")
    private LocalDateTime pointDatetime;

    @ManyToOne
    @JoinColumn(name = "video_id", referencedColumnName = "video_id")
    private Track track;
}
