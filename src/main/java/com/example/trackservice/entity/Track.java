package com.example.trackservice.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "track")
public class Track implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "video_id", unique = true)
    private Long videoId;

    @Column(name = "video_creation_date")
    private LocalDateTime videoCreationDate;

    @ManyToOne
    @JoinColumn(name = "device_id")
    private Device device;
}
