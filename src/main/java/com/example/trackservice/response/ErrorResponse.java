package com.example.trackservice.response;

import lombok.Data;

@Data
public class ErrorResponse {
    private String status;
    private String error;

    public ErrorResponse(String error) {
        status = "ERROR";
        this.error = error;
    }

}
