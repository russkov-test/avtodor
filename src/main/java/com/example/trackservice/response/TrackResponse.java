package com.example.trackservice.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class TrackResponse {
    private Long trackId;
    private Long videoId;
    private LocalDateTime videoCreationDate;
    private String deviceId;
    private Double trackDurationSeconds;
    private Double totalDistanceMeters;
    private Double averageSpeed;
}
