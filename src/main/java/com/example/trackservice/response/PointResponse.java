package com.example.trackservice.response;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class PointResponse {
    private Long id;
    private Double lat;
    private Double lon;
    private Double bearing;
    private Double velocity;
    private LocalDateTime pointDatetime;
}
