package com.example.trackservice.service;

import com.example.trackservice.entity.Point;
import com.example.trackservice.repository.PointRepository;
import com.example.trackservice.response.PointResponse;
import com.example.trackservice.service.converter.PointToPointResponseConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PointService {
    private final PointToPointResponseConverter converter;
    private final PointRepository pointRepository;

    public List<PointResponse> getFilteredPoints(String deviceId, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        if (deviceId != null && startDateTime != null && endDateTime != null) {
            return converterToResponse(pointRepository.findByDeviceIdAndTimePeriod(deviceId, startDateTime, endDateTime));
        } else if (deviceId != null) {
            return converterToResponse(pointRepository.findByDeviceId(deviceId));
        } else if (startDateTime != null && endDateTime != null) {
            return converterToResponse(pointRepository.findByTimePeriod(startDateTime, endDateTime));
        } else {
            return converterToResponse(pointRepository.findAll());
        }
    }

    public Optional<PointResponse> getPointWithMaxSpeedByDeviceId(String deviceId) {
        var point = pointRepository.findPointWithMaxVelocityByDevice(deviceId);
        return point.map(converter::convert);
    }

    private List<PointResponse> converterToResponse(List<Point> points) {
        return points.stream()
                     .map(converter::convert)
                     .collect(Collectors.toList());
    }

}
