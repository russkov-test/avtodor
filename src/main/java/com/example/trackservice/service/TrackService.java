package com.example.trackservice.service;

import com.example.trackservice.repository.TrackRepository;
import com.example.trackservice.response.TrackResponse;
import com.example.trackservice.service.converter.TrackDTOToTrackResponseConverter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TrackService {
    private final TrackDTOToTrackResponseConverter converter;
    private final TrackRepository trackRepository;

    public List<TrackResponse> getTracks() {
        return trackRepository.findTracks()
                              .stream()
                              .map(converter::convert)
                              .collect(Collectors.toList());
    }

}
