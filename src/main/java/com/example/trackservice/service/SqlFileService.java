package com.example.trackservice.service;

import com.example.trackservice.exception.NotFoundException;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class SqlFileService {

    public String getQueryFromFile(String fileName) {
        try {
            return IOUtils.toString(new ClassPathResource(fileName).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new NotFoundException(fileName, e);
        }
    }

}
