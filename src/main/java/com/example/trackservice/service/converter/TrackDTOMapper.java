package com.example.trackservice.service.converter;

import com.example.trackservice.dto.TrackDTO;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;


@Service
public class TrackDTOMapper implements RowMapper<TrackDTO> {

    @Override
    public TrackDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return TrackDTO.builder()
                       .trackId(rs.getLong("track_id"))
                       .videoId(rs.getLong("video_id"))
                       .videoCreationDate(rs.getTimestamp("video_creation_date")
                                            .toLocalDateTime())
                       .deviceId(rs.getString("device_id"))
                       .trackDurationSeconds(rs.getDouble("track_duration_seconds"))
                       .totalDistanceMeters(rs.getDouble("total_distance_meters"))
                       .averageSpeed(rs.getDouble("average_velocity"))
                       .build();
    }

}
