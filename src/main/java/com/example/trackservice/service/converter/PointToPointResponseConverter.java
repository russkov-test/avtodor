package com.example.trackservice.service.converter;

import com.example.trackservice.entity.Point;
import com.example.trackservice.response.PointResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PointToPointResponseConverter implements Converter<Point, PointResponse> {
    @Override
    public PointResponse convert(Point source) {
        return new PointResponse().setId(source.getId())
                                  .setLat(source.getLat())
                                  .setLon(source.getLon())
                                  .setBearing(source.getBearing())
                                  .setVelocity(source.getVelocity())
                                  .setPointDatetime(source.getPointDatetime());
    }
}
