package com.example.trackservice.service.converter;

import com.example.trackservice.dto.TrackDTO;
import com.example.trackservice.response.TrackResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TrackDTOToTrackResponseConverter implements Converter<TrackDTO, TrackResponse> {

    @Override
    public TrackResponse convert(TrackDTO source) {
        return new TrackResponse().setTrackId(source.getTrackId())
                                  .setVideoId(source.getVideoId())
                                  .setVideoCreationDate(source.getVideoCreationDate())
                                  .setDeviceId(source.getDeviceId())
                                  .setTrackDurationSeconds(source.getTrackDurationSeconds())
                                  .setTotalDistanceMeters(source.getTotalDistanceMeters())
                                  .setAverageSpeed(source.getAverageSpeed());
    }

}
