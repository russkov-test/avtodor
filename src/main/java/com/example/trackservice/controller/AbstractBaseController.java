package com.example.trackservice.controller;

import com.example.trackservice.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.DateTimeException;

@Slf4j
@ControllerAdvice
public abstract class AbstractBaseController {
    public static final String FILTERED_POINTS = "/api/points";
    public static final String POINT_BY_MAX_SPEED = "/api/point/max-speed";
    public static final String TRACKS = "/api/tracks";

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> handle(IllegalArgumentException ex) {
        log.error("IllegalArgumentException {}", ex.getMessage());
        return ResponseEntity.badRequest()
                             .body(new ErrorResponse(ex.getMessage()));
    }

    @ExceptionHandler(DateTimeException.class)
    public ResponseEntity<ErrorResponse> handle(DateTimeException ex) {
        log.error("DateTimeException {}", ex.getMessage());
        return ResponseEntity.badRequest()
                             .body(new ErrorResponse(ex.getMessage()));
    }

}
