package com.example.trackservice.controller;

import com.example.trackservice.response.PointResponse;
import com.example.trackservice.service.PointService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@RestController
@RequiredArgsConstructor
public class PointController extends AbstractBaseController {
    private final PointService pointService;

    @GetMapping(FILTERED_POINTS)
    public ResponseEntity<List<PointResponse>> getFilteredPoints(@RequestParam(value = "deviceId", required = false) String deviceId,
                                                                 @RequestParam(value = "startDateTime", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDateTime,
                                                                 @RequestParam(value = "endDateTime", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDateTime) {
        validateParameters(deviceId, startDateTime, endDateTime);
        var points = pointService.getFilteredPoints(deviceId, startDateTime, endDateTime);
        if (points.isEmpty()) {
            return ResponseEntity.noContent()
                                 .build();
        }
        return ResponseEntity.ok(points);
    }

    @GetMapping(POINT_BY_MAX_SPEED)
    public ResponseEntity<PointResponse> getPointWithMaxSpeedByDeviceId(@RequestParam(value = "deviceId") String deviceId) {
        validateDeviceIdLength(deviceId);
        var point = pointService.getPointWithMaxSpeedByDeviceId(deviceId);
        return point.map(ResponseEntity::ok)
                    .orElseGet(() -> ResponseEntity.noContent()
                                                   .build());
    }

    private void validateParameters(String deviceId, LocalDateTime startDateTime, LocalDateTime endDateTime) {
        validateDeviceIdLength(deviceId);
        validateDateTimeParameters(startDateTime, endDateTime);
    }

    private void validateDeviceIdLength(String deviceId) {
        if (nonNull(deviceId) && (deviceId.length() < 16 || deviceId.length() > 32)) {
            throw new IllegalArgumentException("The length of the device ID must be between 16 and 32 characters");
        }
    }

    private void validateDateTimeParameters(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        if ((nonNull(startDateTime) && isNull(endDateTime) || (isNull(startDateTime) && nonNull(endDateTime)))) {
            throw new DateTimeException("Both date parameters must be filled in");
        }

        if (nonNull(startDateTime) && nonNull(endDateTime) && endDateTime.isBefore(startDateTime)) {
            throw new DateTimeException("EndDateTime cannot be earlier than StartDateTime.");
        }
    }

}
