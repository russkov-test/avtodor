package com.example.trackservice.controller;

import com.example.trackservice.response.TrackResponse;
import com.example.trackservice.service.TrackService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class TrackController extends AbstractBaseController {
    private final TrackService trackService;

    @GetMapping(TRACKS)
    public ResponseEntity<List<TrackResponse>> getTracks() {
        var tracks = trackService.getTracks();
        if (tracks.isEmpty()) {
            return ResponseEntity.noContent()
                                 .build();
        }
        return ResponseEntity.ok(tracks);
    }

}
