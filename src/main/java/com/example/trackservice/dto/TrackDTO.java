package com.example.trackservice.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class TrackDTO {
    private Long trackId;
    private Long videoId;
    private LocalDateTime videoCreationDate;
    private String deviceId;
    private Double trackDurationSeconds;
    private Double totalDistanceMeters;
    private Double averageSpeed;
}
