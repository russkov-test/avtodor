package com.example.trackservice.repository.track;

import com.example.trackservice.dto.TrackDTO;
import com.example.trackservice.service.SqlFileService;
import com.example.trackservice.service.converter.TrackDTOMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

@RequiredArgsConstructor
public class CustomTrackRepositoryImpl implements CustomTrackRepository {
    private final SqlFileService sqlFileService;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final TrackDTOMapper mapper;
    private static final String PATH = "sql/track_list.sql";

    @Override
    public List<TrackDTO> findTracks() {
        return jdbcTemplate.query(sqlFileService.getQueryFromFile(PATH), mapper);
    }

}
