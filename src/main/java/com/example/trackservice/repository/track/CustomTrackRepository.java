package com.example.trackservice.repository.track;

import com.example.trackservice.dto.TrackDTO;

import java.util.List;

public interface CustomTrackRepository {
    List<TrackDTO> findTracks();

}
