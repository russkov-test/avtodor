package com.example.trackservice.repository;

import com.example.trackservice.entity.Track;
import com.example.trackservice.repository.track.CustomTrackRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackRepository extends JpaRepository<Track, Long>, CustomTrackRepository {

}
