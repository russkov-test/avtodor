package com.example.trackservice.repository;

import com.example.trackservice.entity.Point;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PointRepository extends JpaRepository<Point, Long> {
    @Query("select p from Point p " +
            "where p.track.device.deviceId = :deviceId " +
            "and p.pointDatetime between :startDateTime and :endDateTime")
    List<Point> findByDeviceIdAndTimePeriod(@Param("deviceId") String deviceId, @Param("startDateTime") LocalDateTime startDateTime, @Param("endDateTime") LocalDateTime endDateTime);

    @Query("select p from Point p " +
            "where p.track.device.deviceId = :deviceId")
    List<Point> findByDeviceId(@Param("deviceId") String deviceId);

    @Query("select p from Point p " +
            "where p.pointDatetime between :startDateTime and :endDateTime")
    List<Point> findByTimePeriod(@Param("startDateTime") LocalDateTime startDateTime, @Param("endDateTime") LocalDateTime endDateTime);

    @Query("select p from Point p " +
            "where p.track.device.deviceId = :deviceId " +
            "and p.velocity = (select max (p2.velocity) from Point p2 where p2.track.device.deviceId = :deviceId)")
    Optional<Point> findPointWithMaxVelocityByDevice(@Param("deviceId") String deviceId);

}
