package com.example.trackservice.controller;

import com.example.trackservice.response.TrackResponse;
import com.example.trackservice.service.TrackService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;

import static com.example.trackservice.controller.AbstractBaseController.TRACKS;
import static java.util.Collections.emptyList;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TrackController.class)
class TrackControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private TrackService trackService;

    @Test
    void getTracks_isEmpty() throws Exception {
        when(trackService.getTracks()).thenReturn(emptyList());

        mockMvc.perform(MockMvcRequestBuilders.get(TRACKS))
               .andExpect(status().isNoContent());
    }

    @Test
    void getTracks_ok() throws Exception {
        var response = new TrackResponse().setTrackId(1L);

        when(trackService.getTracks()).thenReturn(List.of(response));

        mockMvc.perform(MockMvcRequestBuilders.get(TRACKS))
               .andExpect(status().isOk())
               .andExpect(content().json("[{ \"trackId\": 1 }]"));
    }
}