package com.example.trackservice.controller;

import com.example.trackservice.response.PointResponse;
import com.example.trackservice.service.PointService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.example.trackservice.controller.AbstractBaseController.FILTERED_POINTS;
import static com.example.trackservice.controller.AbstractBaseController.POINT_BY_MAX_SPEED;
import static java.util.Collections.emptyList;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(PointController.class)
class PointControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PointService pointService;
    private PointResponse response;
    private final static String JSON_RESPONSE = "[{ \"id\": 1 }]";

    @BeforeEach
    void setUp() {
        response = new PointResponse().setId(1L);
    }

    @Test
    void getFilteredPoints_with_all_parameter_ok() throws Exception {
        when(pointService.getFilteredPoints("1234567890123456", LocalDateTime.parse("2023-06-19T06:00:00"), LocalDateTime.parse("2023-06-19T06:01:00"))).thenReturn(List.of(response));

        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("deviceId", "1234567890123456")
                                              .param("startDateTime", "2023-06-19T06:00:00")
                                              .param("endDateTime", "2023-06-19T06:01:00"))
               .andExpect(status().isOk())
               .andExpect(content().json(JSON_RESPONSE));
    }

    @Test
    void getFilteredPoints_with_dateTime_ok() throws Exception {
        when(pointService.getFilteredPoints(null, LocalDateTime.parse("2023-06-19T06:00:00"), LocalDateTime.parse("2023-06-19T06:01:00"))).thenReturn(List.of(response));

        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("startDateTime", "2023-06-19T06:00:00")
                                              .param("endDateTime", "2023-06-19T06:01:00"))
               .andExpect(status().isOk())
               .andExpect(content().json(JSON_RESPONSE));
    }

    @Test
    void getFilteredPoints_with_dateTime_only_startDateTime_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("startDateTime", "2023-06-19T06:00:00"))
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{ \"status\": \"ERROR\", \"error\": \"Both date parameters must be filled in\" }"));
        verifyNoInteractions(pointService);
    }

    @Test
    void getFilteredPoints_with_dateTime_only_endDateTime_param() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("endDateTime", "2023-06-19T06:00:00"))
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{ \"status\": \"ERROR\", \"error\": \"Both date parameters must be filled in\" }"));
        verifyNoInteractions(pointService);
    }

    @Test
    void getFilteredPoints_with_dateTime_where_end_earlier_start() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("startDateTime", "2023-06-19T06:01:00")
                                              .param("endDateTime", "2023-06-19T06:00:00"))
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{ \"status\": \"ERROR\", \"error\": \"EndDateTime cannot be earlier than StartDateTime.\" }"));
        verifyNoInteractions(pointService);
    }

    @Test
    void getFilteredPoints_with_deviceId_ok() throws Exception {
        when(pointService.getFilteredPoints("1234567890123456", null, null)).thenReturn(List.of(response));

        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("deviceId", "1234567890123456"))
               .andExpect(status().isOk())
               .andExpect(content().json(JSON_RESPONSE));
    }

    @Test
    void getFilteredPoints_with_deviceId_less_16_chars() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("deviceId", "123456789012345"))
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{ \"status\": \"ERROR\", \"error\": \"The length of the device ID must be between 16 and 32 characters\" }"));
        verifyNoInteractions(pointService);
    }

    @Test
    void getFilteredPoints_with_deviceId_more_32_chars() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS)
                                              .param("deviceId", "123456789012345678901234567890123"))
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{ \"status\": \"ERROR\", \"error\": \"The length of the device ID must be between 16 and 32 characters\" }"));
        verifyNoInteractions(pointService);
    }

    @Test
    void getFilteredPoints_no_param_ok() throws Exception {
        when(pointService.getFilteredPoints(null, null, null)).thenReturn(List.of(response));

        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS))
               .andExpect(status().isOk())
               .andExpect(content().json(JSON_RESPONSE));
    }

    @Test
    void getFilteredPoints_no_param_emptyResponse() throws Exception {
        when(pointService.getFilteredPoints(null, null, null)).thenReturn(emptyList());

        mockMvc.perform(MockMvcRequestBuilders.get(FILTERED_POINTS))
               .andExpect(status().isNoContent());
    }

    @Test
    void getPointWithMaxSpeedByDeviceId_ok() throws Exception {
        when(pointService.getPointWithMaxSpeedByDeviceId("12345678901234567")).thenReturn(Optional.of(response));

        mockMvc.perform(MockMvcRequestBuilders.get(POINT_BY_MAX_SPEED)
                                              .param("deviceId", "12345678901234567"))
               .andExpect(status().isOk())
               .andExpect(content().json("{\"id\":1,\"lat\":null,\"lon\":null,\"bearing\":null,\"velocity\":null,\"pointDatetime\":null}"));
    }

    @Test
    void getPointWithMaxSpeedByDeviceId_isEmpty() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(POINT_BY_MAX_SPEED)
                                              .param("deviceId", ""))
               .andExpect(status().isBadRequest())
               .andExpect(content().json("{ \"status\": \"ERROR\", \"error\": \"The length of the device ID must be between 16 and 32 characters\" }"));
        verifyNoInteractions(pointService);
    }

}