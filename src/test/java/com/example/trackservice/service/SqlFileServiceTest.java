package com.example.trackservice.service;

import com.example.trackservice.exception.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SqlFileServiceTest {
    private SqlFileService subj;
    private static final String FILE_IS_EXISTS = "sql/track_list.sql";
    private static final String FILE_NOT_EXISTS = "sql/track_list1.sql";

    @BeforeEach
    void setUp() {
        subj = new SqlFileService();
    }

    @Test
    void getQueryFromFile_ok() {
        var file = subj.getQueryFromFile(FILE_IS_EXISTS);
        assertTrue(file.contains("select"));
    }

    @Test
    void getQueryFromFile_notFound() {
        assertThrows(NotFoundException.class, () -> subj.getQueryFromFile(FILE_NOT_EXISTS));
    }
}