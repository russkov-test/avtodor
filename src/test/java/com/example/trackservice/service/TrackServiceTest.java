package com.example.trackservice.service;

import com.example.trackservice.dto.TrackDTO;
import com.example.trackservice.repository.TrackRepository;
import com.example.trackservice.response.TrackResponse;
import com.example.trackservice.service.converter.TrackDTOToTrackResponseConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TrackServiceTest {
    @InjectMocks
    private TrackService subj;
    @Mock
    private TrackDTOToTrackResponseConverter converter;
    @Mock
    private TrackRepository trackRepository;

    @BeforeEach
    void setUp() {
        subj = new TrackService(converter, trackRepository);
    }

    @Test
    void getTracks_ok() {
        var dateTime = LocalDateTime.of(2023, 10, 1, 10, 0);
        var trackDTO = TrackDTO.builder()
                               .trackId(1L)
                               .videoId(1L)
                               .videoCreationDate(dateTime)
                               .deviceId("1")
                               .trackDurationSeconds(1.0)
                               .totalDistanceMeters(1.0)
                               .averageSpeed(1.0)
                               .build();
        var response = new TrackResponse().setTrackId(1L)
                                          .setVideoId(1L)
                                          .setVideoCreationDate(dateTime)
                                          .setDeviceId("1")
                                          .setTrackDurationSeconds(1.0)
                                          .setTotalDistanceMeters(1.0)
                                          .setAverageSpeed(1.0);
        when(trackRepository.findTracks()).thenReturn(List.of(trackDTO));
        when(converter.convert(trackDTO)).thenReturn(response);

        var tracks = subj.getTracks();
        assertEquals(1, tracks.size());
        assertEquals(response, tracks.get(0));
        verify(trackRepository, times(1)).findTracks();
        verify(converter, times(1)).convert(trackDTO);
    }

    @Test
    void getTracks_empty() {
        when(trackRepository.findTracks()).thenReturn(emptyList());
        var tracks = subj.getTracks();
        assertTrue(tracks.isEmpty());
        verify(trackRepository, times(1)).findTracks();
        verifyNoInteractions(converter);
    }

}