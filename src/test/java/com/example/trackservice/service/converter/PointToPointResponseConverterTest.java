package com.example.trackservice.service.converter;

import com.example.trackservice.entity.Point;
import com.example.trackservice.response.PointResponse;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PointToPointResponseConverterTest {

    @Test
    void convert() {
        var dateTime = LocalDateTime.of(2023, 10, 1, 10, 0);

        var point = new Point();
        point.setId(1L);
        point.setLat(1.1);
        point.setLon(1.1);
        point.setBearing(1.0);
        point.setVelocity(1.0);
        point.setPointDatetime(dateTime);

        var response = new PointResponse().setId(1L)
                                          .setLat(1.1)
                                          .setLon(1.1)
                                          .setBearing(1.0)
                                          .setVelocity(1.0)
                                          .setPointDatetime(dateTime);

        var pointResponse = new PointToPointResponseConverter().convert(point);

        assertEquals(response, pointResponse);
    }
}