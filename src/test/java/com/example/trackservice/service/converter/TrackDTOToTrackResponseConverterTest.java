package com.example.trackservice.service.converter;

import com.example.trackservice.dto.TrackDTO;
import com.example.trackservice.response.TrackResponse;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TrackDTOToTrackResponseConverterTest {

    @Test
    void convert() {
        var dateTime = LocalDateTime.of(2023, 10, 1, 10, 0);

        var trackDTO = TrackDTO.builder()
                               .trackId(1L)
                               .videoId(1L)
                               .videoCreationDate(dateTime)
                               .deviceId("1")
                               .trackDurationSeconds(1.0)
                               .totalDistanceMeters(1.0)
                               .averageSpeed(1.0)
                               .build();
        var response = new TrackResponse().setTrackId(1L)
                                          .setVideoId(1L)
                                          .setVideoCreationDate(dateTime)
                                          .setDeviceId("1")
                                          .setTrackDurationSeconds(1.0)
                                          .setTotalDistanceMeters(1.0)
                                          .setAverageSpeed(1.0);

        TrackResponse trackResponse = new TrackDTOToTrackResponseConverter().convert(trackDTO);

        assertEquals(response, trackResponse);
    }
}