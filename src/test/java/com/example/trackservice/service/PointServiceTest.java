package com.example.trackservice.service;

import com.example.trackservice.entity.Point;
import com.example.trackservice.repository.PointRepository;
import com.example.trackservice.response.PointResponse;
import com.example.trackservice.service.converter.PointToPointResponseConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PointServiceTest {
    @InjectMocks
    private PointService subj;
    @Mock
    private PointToPointResponseConverter converter;
    @Mock
    private PointRepository repository;
    private Point point;
    private PointResponse response;

    @BeforeEach
    void setUp() {
        subj = new PointService(converter, repository);

        var dateTime = LocalDateTime.of(2023, 10, 1, 10, 0);

        point = new Point();
        point.setId(1L);
        point.setLat(1.1);
        point.setLon(1.1);
        point.setBearing(1.0);
        point.setVelocity(1.0);
        point.setPointDatetime(dateTime);

        response = new PointResponse().setId(1L)
                                      .setLat(1.1)
                                      .setLon(1.1)
                                      .setBearing(1.0)
                                      .setVelocity(1.0)
                                      .setPointDatetime(dateTime);
    }

    @Test
    void getFilteredPoints_not_param() {
        when(repository.findAll()).thenReturn(List.of(point));
        when(converter.convert(point)).thenReturn(response);

        var points = subj.getFilteredPoints(null, null, null);

        assertFalse(points.isEmpty());
        assertEquals(response, points.get(0));
        verify(repository, times(1)).findAll();
        verify(converter, times(1)).convert(point);
        verify(repository, never()).findByDeviceIdAndTimePeriod(null, null, null);
        verify(repository, never()).findByTimePeriod(null, null);
        verify(repository, never()).findByDeviceId(null);
    }

    @Test
    void getFilteredPoints_with_device_param() {
        when(repository.findByDeviceId("1")).thenReturn(List.of(point));
        when(converter.convert(point)).thenReturn(response);

        var points = subj.getFilteredPoints("1", null, null);

        assertFalse(points.isEmpty());
        assertEquals(response, points.get(0));
        verify(repository, times(1)).findByDeviceId("1");
        verify(converter, times(1)).convert(point);
        verify(repository, never()).findByDeviceIdAndTimePeriod("1", null, null);
        verify(repository, never()).findByTimePeriod(null, null);
        verify(repository, never()).findAll();
    }

    @Test
    void getFilteredPoints_with_dateTime_param() {
        var startDateTime = LocalDateTime.of(2023, 10, 1, 10, 0);
        var endDateTime = LocalDateTime.of(2023, 10, 1, 10, 1);
        when(repository.findByTimePeriod(startDateTime, endDateTime)).thenReturn(List.of(point));
        when(converter.convert(point)).thenReturn(response);

        var points = subj.getFilteredPoints(null, startDateTime, endDateTime);

        assertFalse(points.isEmpty());
        assertEquals(response, points.get(0));
        verify(repository, times(1)).findByTimePeriod(startDateTime, endDateTime);
        verify(converter, times(1)).convert(point);
        verify(repository, never()).findByDeviceIdAndTimePeriod(null, startDateTime, endDateTime);
        verify(repository, never()).findByDeviceId(null);
        verify(repository, never()).findAll();
    }

    @Test
    void getFilteredPoints_with_all_param() {
        var startDateTime = LocalDateTime.of(2023, 10, 1, 10, 0);
        var endDateTime = LocalDateTime.of(2023, 10, 1, 10, 1);
        when(repository.findByDeviceIdAndTimePeriod("1", startDateTime, endDateTime)).thenReturn(emptyList());

        var points = subj.getFilteredPoints("1", startDateTime, endDateTime);

        assertTrue(points.isEmpty());
        verify(repository, times(1)).findByDeviceIdAndTimePeriod("1", startDateTime, endDateTime);
        verifyNoInteractions(converter);
        verify(repository, never()).findByTimePeriod(startDateTime, endDateTime);
        verify(repository, never()).findByDeviceId(null);
        verify(repository, never()).findAll();
    }

    @Test
    void getPointWithMaxSpeedByDeviceId_with_result() {
        when(repository.findPointWithMaxVelocityByDevice("1")).thenReturn(Optional.of(point));
        when(converter.convert(point)).thenReturn(response);

        var result = subj.getPointWithMaxSpeedByDeviceId("1");

        assertTrue(result.isPresent());
        assertEquals(response, result.get());
        verify(repository, times(1)).findPointWithMaxVelocityByDevice("1");
        verify(converter, times(1)).convert(point);
    }

    @Test
    void getPointWithMaxSpeedByDeviceId_not_result() {
        when(repository.findPointWithMaxVelocityByDevice("1")).thenReturn(Optional.empty());

        var result = subj.getPointWithMaxSpeedByDeviceId("1");

        assertFalse(result.isPresent());
        verify(repository, times(1)).findPointWithMaxVelocityByDevice("1");
        verifyNoInteractions(converter);
    }
}